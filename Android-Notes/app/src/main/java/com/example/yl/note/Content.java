package com.example.yl.note;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;



public class Content extends Activity {

    private Button ok_button, cancle_button;
    private EditText editText;
    private DataBase dataBase;
    private Cursor cursor;
    private int _id = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        init();
    }

    private void init(){
        dataBase = new DataBase(this);
        cursor = dataBase.select();

        ok_button = (Button)findViewById(R.id.button);
        cancle_button = (Button)findViewById(R.id.button2);
        editText = (EditText)findViewById(R.id.editText);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
                Intent intent = new Intent(Content.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        cancle_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Content.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void add(){
        if (editText.getText().toString().equals("")){
            Toast.makeText(Content.this, "Nothing to add", Toast.LENGTH_SHORT).show();
        }else {
            dataBase.insert(editText.getText().toString());

            editText.setText("");
            _id=0;
        }
    }
}
