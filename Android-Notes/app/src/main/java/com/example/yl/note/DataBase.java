package com.example.yl.note;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DataBase extends SQLiteOpenHelper{
    private final static String DB_NAME="my.db";
    private final static int DB_VERSION=1;
    private final static String TBALE_NAME="info";
    public final static String CONTENT="content";
    private final static String ID="_id";
    SQLiteDatabase datebase = getWritableDatabase();

    public DataBase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TBALE_NAME + "(" + ID
                + "  integer  primary key autoincrement," + CONTENT + " text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }

    public long insert(String text){

        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTENT, text);

        long row = datebase.insert(TBALE_NAME, null, contentValues);
        return row;
    }

    public void update(int _id,String text){

        ContentValues contentValues = new ContentValues();
        contentValues.put("content", text);

        datebase.update(TBALE_NAME, contentValues, ID+"=?", new String[]{Integer.toString(_id)});
    }

    public void delete(int _id){
        datebase.delete(TBALE_NAME, ID+"=?",  new String[]{Integer.toString(_id)});
    }

    public Cursor select(){
        Cursor cursor = datebase.query(TBALE_NAME, null, null, null, null, null, null);
        return cursor;
    }



}
