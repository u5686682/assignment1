package com.example.yl.note;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SimpleCursorAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity{

    private ListView listView;
    private Button button_new;
    private Cursor cursor;
    private DataBase dataBase;
    SimpleCursorAdapter adapter;

    private int _id=0;


    String[] items;
    ArrayList<String> listItem;
    EditText editText3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataBase = new DataBase(this);

        cursor = dataBase.select();
        button_new = (Button) findViewById(R.id.button_new);
        editText3 = (EditText)findViewById(R.id.editText3);
        listView = (ListView) findViewById(R.id.listView);

        button_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Content.class);
                startActivity(intent);
                finish();
            }
        });
        initList();

        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    initList();
                } else {
                    searchItem(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public void searchItem(String text){
        for (String item:items){
            if (!item.contains(text)){
                listItem.remove(item);
            }
        }
        adapter.notifyDataSetChanged();
    }

    public void initList(){
        adapter = new SimpleCursorAdapter(MainActivity.this, R.layout.activity_data_base, cursor,
                new String[]{DataBase.CONTENT}, new int[]{R.id.textView});
        listView.setAdapter(adapter);
        listView.invalidateViews();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                cursor.moveToPosition(arg2);

                _id = cursor.getInt(0);
                Intent intent = new Intent(MainActivity.this, Modify.class);
                intent.putExtra("id", _id);
                intent.putExtra("data", cursor.getString(1));
                startActivity(intent);
                finish();
            }
        });
    }



}
