package com.example.yl.note;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Modify extends Activity {

    private EditText editText2;
    private Button button_list, button_delete, button_cancel;
    private DataBase dataBase;
    private Cursor cursor;
    private int _id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify);
        init();
    }

    private void init(){
        editText2 = (EditText)findViewById(R.id.editText2);
        Intent intent = getIntent();
        String data = intent.getStringExtra("data");

        _id = intent.getIntExtra("id", _id);
        editText2.setText(data);

        dataBase = new DataBase(this);

        cursor = dataBase.select();
        button_list = (Button) findViewById(R.id.button3);
        button_delete = (Button)findViewById(R.id.button5);
        button_cancel = (Button)findViewById(R.id.button4);


        button_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
                Intent intent = new Intent(Modify.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Modify.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
                Intent intent = new Intent(Modify.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void update(){
        if (_id==0){
            Toast.makeText(Modify.this,"Nothing to update", Toast.LENGTH_SHORT).show();
        }else {
            dataBase.update(_id, editText2.getText().toString());


            _id = 0;
        }
    }
    public void delete(){
        if (_id==0){
            Toast.makeText(Modify.this,"Nothing to delete", Toast.LENGTH_SHORT).show();
        }else {
            dataBase.delete(_id);

            _id = 0;
        }
    }


}
